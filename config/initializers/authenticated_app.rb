if defined?(AuthenticatedApp)
  AuthenticatedApp.configure do |config|
    config.home_url = :root_url  # home for authenticated users
    config.root_url = :root_url  # home for unauthenticated users
    config.email_only = false    # if true, use email for login.
    config.default_domain = Conf.domain
  end
end
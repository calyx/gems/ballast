Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  if defined?(AuthenticatedApp)
    mount AuthenticatedApp::Engine => "/auth"
  end

  root "home#show"
end

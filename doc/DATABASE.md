Overview
==========================================================

1. Create the database
2. Create `config/database.yml` to configure Rails to use that database.
3. Run `bin/rake setup` to load the scheme and any seed data to the database.

MariaDB
==========================================================

Setting up a Maria database for production:

Install server:

    apt install mariadb-server

Create database and user with support for 4-byte UTF-8:

```sql
cat <<EOF | sudo mariadb -u root
CREATE DATABASE IF NOT EXISTS dbname CHARACTER SET = 'utf8mb4' COLLATE = 'utf8mb4_general_ci';
USE mysql;
CREATE OR REPLACE USER username@localhost IDENTIFIED BY 'passw0rd';
GRANT ALL PRIVILEGES ON dbname.* TO username@localhost;
FLUSH PRIVILEGES;
EOF
```
Replace "dbname", "username", and "passw0rd" with the values of your choice.

Create `config/database.yml`:

```yaml
production:
  adapter: mysql2
  encoding: utf8mb4
  collation: utf8mb4_unicode_ci
  database: dbname
  username: username
  password: passw0rd
  host: localhost
```

Alternately, use environment variable DATABASE_URL

    DATABASE_URL=mysql2://user:password@127.0.0.1/db_name


PostgreSQL
==========================================================

To be written

SQLite
==========================================================

Ensure the gem "sqlite3" is defined in your Gemfile.

`config/database.yml`:

```yaml
default: &default
  adapter: sqlite3
  timeout: 5000

development:
  <<: *default
  database: db/development.sqlite3

test:
  <<: *default
  database: db/test.sqlite3

production:
  <<: *default
  database: db/production.sqlite3
```
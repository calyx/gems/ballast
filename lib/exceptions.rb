class ErrorMessage < StandardError
end

class ForbiddenRequest < StandardError
end

class SuspendedRequest < StandardError
end

class UnauthenticatedRequest < StandardError
end

class UnauthorizedRequest < StandardError
end
require_relative '../task_helper'

class BallastTask < TaskHelper
  def update
    if git_branch != 'main'
      bad("checkout branch 'main' before updating Ballast")
    else
      stash do
        run 'git', 'fetch', 'upstream_ballast', 'main'
        run 'git', 'merge', 'upstream_ballast/main'
      end
      ok("Rails Ballast updated")
    end
  end

  def git_branch
    `git rev-parse --abbrev-ref HEAD`.strip
  end
end

namespace :ballast do
  desc "Merges in new changes in upstream Rails Ballast"
  task :update do
    BallastTask.new.update
  end
end

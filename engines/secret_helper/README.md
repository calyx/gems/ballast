## Secret Helper

Various methods for working with encrypted URL strings, random codes, and signed tickets.

### Encrypted URLs

    encrypted_parameter = SecretURL::encrypt('hello')
    cleartext = SecretURL::decrypt(encrypted_parameter)
    puts cleartext
    > "hello"

### Random Codes

    puts RandomCode.create(10)
    > "zgasbkkk6r"

    puts RandomCode.create(12, 4)
    > "dqbj-ytc4-978t"

module NestHelper
  module IconHelper

    #
    # accepted options:
    #
    # class:
    # fill:
    # height:
    # width:
    #
    def icon(*args, &block)
      symbol, label, options = parse_icon_arguments(*args, &block)
      svg = icon_svg(symbol, options)
      if label
        label = ERB::Util.html_escape_once(label) unless label.html_safe?
        "#{svg} #{label}".html_safe
      else
        svg
      end
    end

    private

    def icon_svg(symbol, options)
      if defined?(bootstrap_icon)
        bootstrap_icon(symbol, options||{})
      end
    end

    def parse_icon_arguments(*args, &block)
      symbol  = args.first.to_s
      label   = nil
      options = nil
      if args[1].is_a? String
        label   = args[1]
        options = args[2]
      else
        options = args[1]
      end
      if block_given?
        label = yield
      end
      return [symbol, label, options]
    end
  end
end

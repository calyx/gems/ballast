# see
# https://guides.rubyonrails.org/configuring.html#rails-railtie-initializer
# https://api.rubyonrails.org/classes/ActiveSupport/LazyLoadHooks.html

require_relative 'helper'
require_relative 'controller_concern'

module Theme
  class Engine < ::Rails::Engine
    isolate_namespace Theme

    config.to_prepare do
      ActiveSupport.on_load(:action_controller) do
        include Theme::ControllerConcern
      end
      ActiveSupport.on_load(:action_view) do
        include Theme::Helper
      end
    end

    initializer "theme.assets.precompile" do |app|
      app.config.assets.precompile << "theme/application.css"
      Dir.chdir(File.expand_path('../../../app/assets/', __FILE__)) do
        app.config.assets.precompile += Dir["fonts/*.woff2"]
        app.config.assets.precompile += Dir["images/**/*.{svg,png,ico,jpg}"]
      end
    end
  end
end
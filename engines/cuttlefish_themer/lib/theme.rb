require_relative 'theme/configuration'
require_relative 'theme/default'
if defined?(Rails::Engine)
  require_relative 'theme/engine'
else
  require "bootstrap"
  require "zeitwerk"
  loader = Zeitwerk::Loader.for_gem
  loader.setup
end

module Theme
  def self.configure
    yield config
  end

  def self.config
    @config ||= Configuration.new
  end
end

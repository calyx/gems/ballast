##
## Here lies the default gems always included by Rails Ballast.
## Rather than modify this file, it is easier instead to modify
## Gemfile.first or Gemfile.last and keep this file managed
## by upstream Ballast.
##

# defaults that may be modified in Gemfile.conf
$source        = "https://rubygems.org"
$ruby_version  = nil
$javascript    = true
$gemfile_first = File.expand_path('../Gemfile.first', __FILE__)
$gemfile_last  = File.expand_path('../Gemfile.last', __FILE__)

if File.exist?($gemfile_first)
  eval(File.new($gemfile_first).read)
end

source $source
ruby $ruby_version if $ruby_version

##
## DEFAULT CORE RAILS
##

gem "rails", "~> 7.0.5", ">= 7.0.5"
gem "sqlite3", "~> 1.4"
gem "puma", "~> 5.0"
gem "bootsnap", require: false

##
## DEFAULT RAILS HELPERS
##

gem "kaminari"

##
## DEFAULT ASSETS
##

gem "sprockets-rails"
gem "sassc-rails"
gem "haml-rails"
gem "bootstrap"
gem "bootstrap-icons-helper"

##
## DEFAULT JAVASCRIPT
##

if $javascript
  gem "importmap-rails"
  gem "turbo-rails"
  gem "stimulus-rails"
  gem "jbuilder"
end

##
## DEFAULT TESTING AND DEVELOPMENT
##

group :development, :test do
  gem "debug", platforms: %i[ mri mingw x64_mingw ]
end
group :development do
  gem "web-console"  # debugging console in browser
  gem "listen"       # smartly reload classes when files change
end
group :test do
  #gem "capybara"
  #gem "selenium-webdriver"
  #gem "webdrivers"
end

##
## CUSTOM APP GEMS
##

if File.exist?($gemfile_last)
  require($gemfile_last)
end

##
## LOCAL GEMS / ENGINES
##

Dir.glob('engines/*/*.gemspec').each do |gemspec_path|
  gem_name = File.basename(gemspec_path).sub('.gemspec', '')
  gem_path = File.dirname(gemspec_path)
  gem gem_name, path: gem_path
end
